<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Seance
 *
 * @ORM\Table(name="seance")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\SeanceRepository")
 */
class Seance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=4000)
     */
    private $contenu;


    /**
     * @ORM\ManyToOne(targetEntity="Cours", inversedBy="seances")
     * @ORM\JoinColumn(name="cours_id", referencedColumnName="id",nullable=false, onDelete="CASCADE")
     */
    private $cours;

    /**
     * @ORM\OneToMany(targetEntity="Suivi", mappedBy="seance")
     */
    private $suivi;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="seance")
     */
    private $question;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Seance
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Seance
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }


    /**
     * Set user
     *
     * @param string $cours
     *
     * @return Seance
     */
    public function setCours($cours)
    {
        $this->cours = $cours;

        return $this;
    }

    /**
     * Get cours
     *
     * @return string
     */
    public function getCours()
    {
        return $this->cours;
    }

    public function __construct()
    {
        $this->suivi = new ArrayCollection();
        $this->question = new ArrayCollection();
    }

    public function getSuivi()
    {
        return $this->suivi;
    }
    public function getQuestion()
    {
        return $this->question;
    }
}

