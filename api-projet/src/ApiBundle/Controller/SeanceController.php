<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 04/07/2018
 * Time: 13:12
 */

namespace ApiBundle\Controller;




use ApiBundle\Entity\Seance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle


use Nelmio\ApiDocBundle\Annotation as Doc;

Class SeanceController extends Controller
{
    /**
     * @Rest\View(serializerGroups={"seances"})
     * @Rest\Get("/seances")
     *
     *
     * @Doc\ApiDoc(
     *     section="Seances",
     *     resource=true,
     *     description="Get the list of all seances."
     * )
     */
    public function getSeanceAction(Request $request)
    {
        $seance = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Seance')
            ->findAll();
        /* @var  $seance Seance[] */

        return $seance;
    }


    /**
     * @Rest\View(serializerGroups={"seances"})
     * @Rest\Get("/seances/{id}")
     *
     * @Doc\ApiDoc(
     *     section="Seances",
     *     resource=true,
     *     description="Get one seances.",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The article unique identifier."
     *         }
     *     }
     * )
     */

    public function getUniqueSeanceAction($id, Request $request)
    {

        $seance = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Seance')
            ->find($id);


        if (empty($seance)) {
            return new JsonResponse(array('message' => 'Seance not found'), Response::HTTP_NOT_FOUND);
        }

        return $seance;
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED,serializerGroups={"seances"})
     * @Rest\Post("/seances")
     * @Doc\ApiDoc(
     *     section="Seances",
     *     resource=true,
     *     description="Post seance.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function postSeanceAction(Request $request)
    {
        $seance = new Seance();
        $form = $this->createForm('ApiBundle\Form\Type\SeanceType',$seance);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($seance);
            $em->flush();
            return $seance;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT,serializerGroups={"seances"})
     * @Rest\Delete("/seances/{id}")
     * @Doc\ApiDoc(
     *     section="Seances",
     *     resource=true,
     *     description="remove seance.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function removeSeanceAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $seance = $em->getRepository('ApiBundle:Seance')
            ->find($request->get('id'));
        /* @var $seance Seance */

        if ($seance) {
            $em->remove($seance);
            $em->flush();
        }

    }

    /**
     * @Rest\View(serializerGroups={"seances"})
     * @Rest\Put("/seances/{id}")
     * @Doc\ApiDoc(
     *     section="Seances",
     *     resource=true,
     *     description="update seance."
     *
     * )
     */
    public function patchSeanceAction(Request $request)
    {
        return $this->updateSeance($request);
    }

    private function updateSeance(Request $request)
    {
        $seance = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Seance')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $seance Seance */

        if (empty($seance)) {
            return new JsonResponse(array('message' => 'Seance not found'), Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm('ApiBundle\Form\Type\SeanceType', $seance);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($seance);
            $em->flush();
            return $seance;
        } else {
            return $form;
        }


    }
}