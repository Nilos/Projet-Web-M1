<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 04/07/2018
 * Time: 13:18
 */


namespace ApiBundle\Controller;




use ApiBundle\Entity\Suivi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use ApiBundle\Entity\Chapitre;

use Nelmio\ApiDocBundle\Annotation as Doc;

Class SuiviController extends Controller
{
    /**
     * @Rest\View(serializerGroups={"suivi"})
     * @Rest\Get("/suivi")
     *
     *
     * @Doc\ApiDoc(
     *     section="Suivi",
     *     resource=true,
     *     description="Get the list of all suivi."
     * )
     */
    public function getSuiviAction(Request $request)
    {
        $suivi = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Suivi')
            ->findAll();
        /* @var  $suivi Suivi[] */

        return $suivi;
    }


    /**
     * @Rest\View(serializerGroups={"suivi"})
     * @Rest\Get("/suivi/{id}")
     *
     * @Doc\ApiDoc(
     *     section="Suivi",
     *     resource=true,
     *     description="Get one suivi.",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The article unique identifier."
     *         }
     *     }
     * )
     */

    public function getUniqueSuiviAction($id, Request $request)
    {

        $suivi = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Suivi')
            ->find($id);


        if (empty($suivi)) {
            return new JsonResponse(array('message' => 'Suivi not found'), Response::HTTP_NOT_FOUND);
        }

        return $suivi;
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED,serializerGroups={"suivi"})
     * @Rest\Post("/suivi")
     * @Doc\ApiDoc(
     *     section="Suivi",
     *     resource=true,
     *     description="Post suivi.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function postSuiviAction(Request $request)
    {
        $suivi = new Suivi();
        $form = $this->createForm('ApiBundle\Form\Type\SuiviType',$suivi);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($suivi);
            $em->flush();
            return $suivi;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT,serializerGroups={"suivi"})
     * @Rest\Delete("/suivi/{id}")
     * @Doc\ApiDoc(
     *     section="Suivi",
     *     resource=true,
     *     description="remove suivi.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function removeSuiviAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $suivi = $em->getRepository('ApiBundle:Chapitre')
            ->find($request->get('id'));
        /* @var $suivi Suivi */

        if ($suivi) {
            $em->remove($suivi);
            $em->flush();
        }

    }

    /**
     * @Rest\View(serializerGroups={"suivi"})
     * @Rest\Put("/suivi/{id}")
     * @Doc\ApiDoc(
     *     section="Suivi",
     *     resource=true,
     *     description="update suivi."
     *
     * )
     */
    public function patchSuiviAction(Request $request)
    {
        return $this->updateSuivi($request);
    }

    private function updateSuivi(Request $request)
    {
        $suivi = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Suivi')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $suivi Suivi */

        if (empty($suivi)) {
            return new JsonResponse(array('message' => 'Suivi not found'), Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm('ApiBundle\Form\Type\SuiviType', $suivi);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($suivi);
            $em->flush();
            return $suivi;
        } else {
            return $form;
        }


    }
}