<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 04/07/2018
 * Time: 11:29
 */


namespace ApiBundle\Controller;




use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use ApiBundle\Entity\User;
use ApiBundle\Form\Type\UserType;

use Nelmio\ApiDocBundle\Annotation as Doc;

Class UserController extends Controller
{
    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Get("/users")
     *
     *
     * @Doc\ApiDoc(
     *     section="Users",
     *     resource=true,
     *     description="Get the list of all users."
     * )
     */
    public function getUsersAction(Request $request)
    {
        $users =$this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:User')
            ->findAll();
        /* @var $users User[]*/

        return $users;
    }


    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Get("/users/{id}")
     *
     * @Doc\ApiDoc(
     *     section="Users",
     *     resource=true,
     *     description="Get one user.",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The article unique identifier."
     *         }
     *     }
     * )
     */

    public function getUniqueUserAction($id,Request $request)
    {

        $user=$this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:User')
            ->find($id);


        if (empty($user)) {
            return new JsonResponse(array('message' => 'User not found'), Response::HTTP_NOT_FOUND);
        }

        return $user;
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED,serializerGroups={"user"})
     * @Rest\Post("/users")
     * @Doc\ApiDoc(
     *     section="Users",
     *     resource=true,
     *     description="Post users.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function postUsersAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('ApiBundle\Form\Type\UserType',$user);
        $form->submit($request->request->all());


        if ($form -> isValid()){

            $encoder = $this->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encoded);
            $em=$this->get('doctrine.orm.entity_manager');
            $em->persist($user);
            $em->flush();
            return $user;
        }else{
            return $form;
        }
    }


    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Delete("/users/{id}")
     *  @Doc\ApiDoc(
     *     section="Users",
     *     resource=true,
     *     description="remove users.",
     *     statusCodes={
     *         200="Returned when erased",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function removeUsersAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('ApiBundle:User')
            ->find($request->get('id'));
        /* @var $user User */

        if ($user) {
            $em->remove($user);
            $em->flush();
        }

    }
    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Put("/users/{id}")
     *  @Doc\ApiDoc(
     *     section="Users",
     *     resource=true,
     *     description="update users."
     *
     * )
     */
    public function patchUserAction(Request $request)
    {
        return $this->updateUser($request);
    }
    private function updateUser(Request $request)
    {
        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:User')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $user User */



        if (empty($user)) {
            return new JsonResponse(array('message' => 'User not found'), Response::HTTP_NOT_FOUND);
        }



        $form = $this->createForm('ApiBundle\Form\Type\UserType', $user);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($user);
            $em->flush();
            return $user;
        } else {
            return $form;
        }


    }
}