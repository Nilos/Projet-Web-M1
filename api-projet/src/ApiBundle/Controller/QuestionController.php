<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 04/07/2018
 * Time: 13:01
 */


namespace ApiBundle\Controller;




use ApiBundle\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use ApiBundle\Entity\Chapitre;

use Nelmio\ApiDocBundle\Annotation as Doc;

Class QuestionController extends Controller
{
    /**
     * @Rest\View(serializerGroups={"question"})
     * @Rest\Get("/questions")
     *
     *
     * @Doc\ApiDoc(
     *     section="Questions",
     *     resource=true,
     *     description="Get the list of all questions."
     * )
     */
    public function getQuestionsAction(Request $request)
    {
        $questions = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Question')
            ->findAll();
        /* @var  $questions Question[] */

        return $questions;
    }


    /**
     * @Rest\View(serializerGroups={"question"})
     * @Rest\Get("/questions/{id}")
     *
     * @Doc\ApiDoc(
     *     section="Questions",
     *     resource=true,
     *     description="Get one question.",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The article unique identifier."
     *         }
     *     }
     * )
     */

    public function getUniqueQuesitonAction($id, Request $request)
    {

        $question = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Question')
            ->find($id);


        if (empty($question)) {
            return new JsonResponse(array('message' => 'Question not found'), Response::HTTP_NOT_FOUND);
        }

        return $question;
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED,serializerGroups={"question"})
     * @Rest\Post("/questions")
     * @Doc\ApiDoc(
     *     section="Questions",
     *     resource=true,
     *     description="Post question.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function postQuestionAction(Request $request)
    {
        $question = new Chapitre();
        $form = $this->createForm('ApiBundle\Form\Type\QustionType',$question);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($question);
            $em->flush();
            return $question;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT,serializerGroups={"question"})
     * @Rest\Delete("/questions/{id}")
     * @Doc\ApiDoc(
     *     section="Questions",
     *     resource=true,
     *     description="remove question.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function removeQuestionAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $question = $em->getRepository('ApiBundle:Question')
            ->find($request->get('id'));
        /* @var $question Question */

        if ($question) {
            $em->remove($question);
            $em->flush();
        }

    }

    /**
     * @Rest\View(serializerGroups={"question"})
     * @Rest\Put("/questions/{id}")
     * @Doc\ApiDoc(
     *     section="Questions",
     *     resource=true,
     *     description="update question."
     *
     * )
     */
    public function patchQuestionAction(Request $request)
    {
        return $this->updateQuestion($request);
    }

    private function updateQuestion(Request $request)
    {
        $question = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Question')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $question Question */

        if (empty($question)) {
            return new JsonResponse(array('message' => 'Question not found'), Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm('ApiBundle\Form\Type\QuestionType', $question);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($question);
            $em->flush();
            return $question;
        } else {
            return $form;
        }


    }
}