<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 03/07/2018
 * Time: 16:08
 */


namespace ApiBundle\Controller;




use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use ApiBundle\Entity\Cours;

use Nelmio\ApiDocBundle\Annotation as Doc;

Class CoursController extends Controller
{
    /**
     * @Rest\View(serializerGroups={"cours"})
     * @Rest\Get("/cours")
     *
     *
     * @Doc\ApiDoc(
     *     section="Cours",
     *     resource=true,
     *     description="Get the list of all cours."
     * )
     */
    public function getCoursAction(Request $request)
    {
        $cours =$this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Cours')
            ->findAll();
        /* @var $cours Cours[]*/

        return $cours;
    }


    /**
     * @Rest\View(serializerGroups={"cours"})
     * @Rest\Get("/cours/{id}")
     *
     * @Doc\ApiDoc(
     *     section="Cours",
     *     resource=true,
     *     description="Get one cours.",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The article unique identifier."
     *         }
     *     }
     * )
     */

    public function getUniqueCoursAction($id,Request $request)
    {

        $cours=$this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Cours')
            ->find($id);


        if (empty($cours)) {
            return new JsonResponse(array('message' => 'Cours not found'), Response::HTTP_NOT_FOUND);
        }

        return $cours;
    }


    /**
     * @Rest\View(serializerGroups={"cours"})
     * @Rest\Post("/cours")
     * @Doc\ApiDoc(
     *     section="Cours",
     *     resource=true,
     *     description="Post cours.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function postCoursAction(Request $request)
    {
        $cours = new Cours();
        $form = $this -> createForm('ApiBundle\Form\Type\CoursType',$cours);
        $form->submit($request->request->all());

        if ($form -> isValid()){
            $em=$this->get('doctrine.orm.entity_manager');
            $em->persist($cours);
            $em->flush();
            return $cours;
        }else{
            return $form;
        }
    }


    /**
     * @Rest\View(serializerGroups={"cours"})
     * @Rest\Delete("/cours/{id}")
     *  @Doc\ApiDoc(
     *     section="Cours",
     *     resource=true,
     *     description="remove cours.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function removecoursAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $cours = $em->getRepository('ApiBundle:Cours')
            ->find($request->get('id'));
        /* @var $cours Cours */

        if ($cours) {
            $em->remove($cours);
            $em->flush();
        }

    }
    /**
     * @Rest\View(serializerGroups={"cours"})
     * @Rest\Put("/cours/{id}")
     *  @Doc\ApiDoc(
     *     section="Cours",
     *     resource=true,
     *     description="update cours."
     *
     * )
     */
    public function patchCoursAction(Request $request)
    {
        return $this->updateCours($request);
    }
    private function updateCours(Request $request)
    {
        $cours = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Cours')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $cours Cours */

        if (empty($cours)) {
            return new JsonResponse(array('message' => 'Cours not found'), Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm('ApiBundle\Form\Type\CoursType', $cours);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($cours);
            $em->flush();
            return $cours;
        } else {
            return $form;
        }


    }
}