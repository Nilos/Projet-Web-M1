<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 04/07/2018
 * Time: 13:06
 */
namespace ApiBundle\Controller;




use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use ApiBundle\Entity\Reponse;

use Nelmio\ApiDocBundle\Annotation as Doc;

Class ReponseController extends Controller
{
    /**
     * @Rest\View(serializerGroups={"reponse"})
     * @Rest\Get("/reponses")
     *
     *
     * @Doc\ApiDoc(
     *     section="Reponses",
     *     resource=true,
     *     description="Get the list of all responses."
     * )
     */
    public function getReponsesAction(Request $request)
    {
        $reponse = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Reponse')
            ->findAll();
        /* @var  $reponse Reponse[] */

        return $reponse;
    }


    /**
     * @Rest\View(serializerGroups={"reponse"})
     * @Rest\Get("/reponses/{id}")
     *
     * @Doc\ApiDoc(
     *     section="Reponses",
     *     resource=true,
     *     description="Get one reponse.",
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirements"="\d+",
     *             "description"="The article unique identifier."
     *         }
     *     }
     * )
     */

    public function getUniqueReponseAction($id, Request $request)
    {

        $response = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Reponse')
            ->find($id);


        if (empty($reponse)) {
            return new JsonResponse(array('message' => 'Reponse not found'), Response::HTTP_NOT_FOUND);
        }

        return $reponse;
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED,serializerGroups={"reponse"})
     * @Rest\Post("/reponses")
     * @Doc\ApiDoc(
     *     section="Reponses",
     *     resource=true,
     *     description="Post reponse.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function postReponseAction(Request $request)
    {
        $reponse = new Reponse();
        $form = $this->createForm('ApiBundle\Form\Type\ReponseType',$reponse);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($reponse);
            $em->flush();
            return $reponse;
        } else {
            return $form;
        }
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT,serializerGroups={"reponse"})
     * @Rest\Delete("/reponses/{id}")
     * @Doc\ApiDoc(
     *     section="Reponses",
     *     resource=true,
     *     description="remove reponse.",
     *     statusCodes={
     *         201="Returned when created",
     *         400="Returned when a violation is raised by validation"
     *     }
     *
     * )
     */
    public function removeReponseAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $reponse = $em->getRepository('ApiBundle:Reponse')
            ->find($request->get('id'));
        /* @var $reponse Reponse */

        if ($reponse) {
            $em->remove($reponse);
            $em->flush();
        }

    }

    /**
     * @Rest\View(serializerGroups={"reponse"})
     * @Rest\Put("/reponses/{id}")
     * @Doc\ApiDoc(
     *     section="Reponses",
     *     resource=true,
     *     description="update reponse."
     *
     * )
     */
    public function patchReponseAction(Request $request)
    {
        return $this->updateReponse($request);
    }

    private function updateReponse(Request $request)
    {
        $reponse = $this->get('doctrine.orm.entity_manager')
            ->getRepository('ApiBundle:Reponse')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $reponse Reponses */

        if (empty($reponse)) {
            return new JsonResponse(array('message' => 'Reponse not found'), Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm('ApiBundle\Form\Type\ReponseType', $reponse);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($reponse);
            $em->flush();
            return $reponse;
        } else {
            return $form;
        }


    }
}