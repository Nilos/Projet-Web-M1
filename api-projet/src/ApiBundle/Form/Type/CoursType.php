<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 04/07/2018
 * Time: 10:45
 */



namespace ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use ApiBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CoursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre')
            ->add('description')
            ->add('image')
            ->add('actif')
            ->add('user', EntityType::class, array(
                'class' => User::class,
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Cours',
            'csrf_protection' => false,
            'allow_extra_fields'=>true
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getNom()
    {
        return 'cours';
    }
}