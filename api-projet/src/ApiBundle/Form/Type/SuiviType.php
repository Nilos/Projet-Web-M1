<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 04/07/2018
 * Time: 11:24
 */
namespace ApiBundle\Form\Type;

use ApiBundle\Entity\Seance;
use ApiBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SuiviType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commentaire')
            ->add('note')
            ->add('created_at')
            ->add('seance', EntityType::class, array(
                'class' => Seance::class,
            ))
            ->add('user', EntityType::class, array(
                'class' => User::class,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Suivi',
            'csrf_protection' => false,
            'allow_extra_fields'=>true
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getNom()
    {
        return 'Suivi';
    }
}