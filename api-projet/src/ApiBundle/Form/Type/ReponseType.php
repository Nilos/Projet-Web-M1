<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 04/07/2018
 * Time: 11:22
 */


namespace ApiBundle\Form\Type;

use ApiBundle\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ReponseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle')
            ->add('correct')
            ->add('question', EntityType::class, array(
                'class' => Question::class,
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Reponse',
            'csrf_protection' => false,
            'allow_extra_fields'=>true
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getNom()
    {
        return 'Response';
    }
}