<?php
/**
 * Created by PhpStorm.
 * User: ayoub
 * Date: 06/07/2018
 * Time: 01:04
 */

namespace ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CredentialsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('login');
        $builder->add('password');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Credentials',
            'csrf_protection' => false,
            'allow_extra_fields'=>true,
        ));
    }
}