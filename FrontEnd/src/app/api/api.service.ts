import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { Http, Response, Headers } from '@angular/http';
import { Courses } from '../model/courses';
import { Chapter } from '../model/chapter';
import { User } from '../model/user';
import { Meeting } from '../model/meeting';
import { Question } from '../model/question';
import { Reponse } from '../model/reponse';
import { Follow } from '../model/follow';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const API_URL = environment.apiUrl;


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor( private http: Http ) { }

  public getAllCourses(): Observable<Courses[]> {
    return this.http
      .get(API_URL + '/cours')
      .map(response => {
        const courses = response.json();
        return courses.map((courses) => new Courses(courses));
      })
      .catch(this.handleError);
  }

  public createCourses(courses: Courses): Observable<Courses> {
    return this.http
      .post(API_URL + '/cours', courses)
      .map(response => {
        return new Courses(response.json());
      })
      .catch(this.handleError);
  }

  public getCoursesById(coursesId: number): Observable<Courses> {
    return this.http
      .get(API_URL + '/cours/' + coursesId)
      .map(response => {
        return new Courses(response.json());
      })
      .catch(this.handleError);
  }

  public updateCourses(courses: Courses): Observable<Courses> {
    return this.http
      .put(API_URL + '/cours/' + courses.id, courses)
      .map(response => {
        return new Courses(response.json());
      })
      .catch(this.handleError);
  }

  public deleteCoursesById(coursesId: number): Observable<null> {
    return this.http
      .delete(API_URL + '/cours/' + coursesId)
      .map(response => null)
      .catch(this.handleError);
  }

  public getAllChapters(): Observable<Chapter[]> {
    return this.http
      .get(API_URL + '/chapitres')
      .map(response => {
        const chapters = response.json();
        return chapters.map((chapters) => new Chapter(chapters));
      })
      .catch(this.handleError);
  }

  public createChapter(chapter: Chapter): Observable<Chapter> {
    return this.http
      .post(API_URL + '/chapitres', chapter)
      .map(response => {
        return new Chapter(response.json());
      })
      .catch(this.handleError);
  }

  public getChapterById(chapterId: number): Observable<Chapter> {
    return this.http
      .get(API_URL + '/chapitres/' + chapterId)
      .map(response => {
        return new Chapter(response.json());
      })
      .catch(this.handleError);
  }

  public updateChapter(chapter: Chapter): Observable<Chapter> {
    return this.http
      .put(API_URL + '/chapitres/' + chapter.id, chapter)
      .map(response => {
        return new Chapter(response.json());
      })
      .catch(this.handleError);
  }

  public deleteChapterById(chapterId: number): Observable<null> {
    return this.http
      .delete(API_URL + '/chapitres/' + chapterId)
      .map(response => null)
      .catch(this.handleError);
  }

  public getAllUsers(): Observable<User[]> {
    return this.http
      .get(API_URL + '/users')
      .map(response => {
        const users = response.json();
        return users.map((users) => new User(users));
      })
      .catch(this.handleError);
  }

  public createUser(user: User): Observable<User> {
    return this.http
      .post(API_URL + '/users', user)
      .map(response => {
        return new User(response.json());
      })
      .catch(this.handleError);
  }

  public getUserById(userId: number): Observable<User> {
    return this.http
      .get(API_URL + '/users/' + userId)
      .map(response => {
        return new User(response.json());
      })
      .catch(this.handleError);
  }

  public updateUser(user: User): Observable<User> {
    return this.http
      .put(API_URL + '/users/' + user.id, user)
      .map(response => {
        return new User(response.json());
      })
      .catch(this.handleError);
  }

  public deleteUserById(userId: number): Observable<null> {
    return this.http
      .delete(API_URL + '/users/' + userId)
      .map(response => null)
      .catch(this.handleError);
  }

  public getAllMeetings(): Observable<Meeting[]> {
    return this.http
      .get(API_URL + '/seances')
      .map(response => {
        const meetings = response.json();
        return meetings.map((meetings) => new Meeting(meetings));
      })
      .catch(this.handleError);
  }

  public createMeeting(meeting: Meeting): Observable<Meeting> {
    return this.http
      .post(API_URL + '/seances', meeting)
      .map(response => {
        return new Meeting(response.json());
      })
      .catch(this.handleError);
  }

  public getMeetingById(meetingId: number): Observable<Meeting> {
    return this.http
      .get(API_URL + '/seances/' + meetingId)
      .map(response => {
        return new Meeting(response.json());
      })
      .catch(this.handleError);
  }

  public updateMeeting(meeting: Meeting): Observable<Meeting> {
    return this.http
      .put(API_URL + '/seances/' + meeting.id, meeting)
      .map(response => {
        return new Meeting(response.json());
      })
      .catch(this.handleError);
  }

  public deleteMeetingById(meetingId: number): Observable<null> {
    return this.http
      .delete(API_URL + '/seances/' + meetingId)
      .map(response => null)
      .catch(this.handleError);
  }

  public getAllQuestions(): Observable<Question[]> {
    return this.http
      .get(API_URL + '/questions')
      .map(response => {
        const questions = response.json();
        return questions.map((questions) => new Question(questions));
      })
      .catch(this.handleError);
  }

  public createQuestion(question: Question): Observable<Question> {
    return this.http
      .post(API_URL + '/questions', question)
      .map(response => {
        return new Question(response.json());
      })
      .catch(this.handleError);
  }

  public getQuestionById(questionId: number): Observable<Question> {
    return this.http
      .get(API_URL + '/questions/' + questionId)
      .map(response => {
        return new Question(response.json());
      })
      .catch(this.handleError);
  }

  public updateQuestion(question: Question): Observable<Question> {
    return this.http
      .put(API_URL + '/questions/' + question.id, question)
      .map(response => {
        return new Question(response.json());
      })
      .catch(this.handleError);
  }

  public deleteQuestionById(questionId: number): Observable<null> {
    return this.http
      .delete(API_URL + '/questions/' + questionId)
      .map(response => null)
      .catch(this.handleError);
  }

  public getAllReponses(): Observable<Reponse[]> {
    return this.http
      .get(API_URL + '/reponses')
      .map(response => {
        const reponses = response.json();
        return reponses.map((reponses) => new Reponse(reponses));
      })
      .catch(this.handleError);
  }

  public createReponse(reponse: Reponse): Observable<Reponse> {
    return this.http
      .post(API_URL + '/reponses', reponse)
      .map(response => {
        return new Reponse(response.json());
      })
      .catch(this.handleError);
  }

  public getReponseById(reponseId: number): Observable<Reponse> {
    return this.http
      .get(API_URL + '/reponses/' + reponseId)
      .map(response => {
        return new Reponse(response.json());
      })
      .catch(this.handleError);
  }

  public updateReponse(reponse: Reponse): Observable<Reponse> {
    return this.http
      .put(API_URL + '/reponses/' + reponse.id, reponse)
      .map(response => {
        return new Reponse(response.json());
      })
      .catch(this.handleError);
  }

  public deleteReponseById(reponseId: number): Observable<null> {
    return this.http
      .delete(API_URL + '/reponses/' + reponseId)
      .map(response => null)
      .catch(this.handleError);
  }

  public getAllFollows(): Observable<Follow[]> {
    return this.http
      .get(API_URL + '/suivi')
      .map(response => {
        const follows = response.json();
        return follows.map((follows) => new Follow(follows));
      })
      .catch(this.handleError);
  }

  public createFollow(follow: Follow): Observable<Follow> {
    return this.http
      .post(API_URL + '/suivi', follow)
      .map(response => {
        return new Follow(response.json());
      })
      .catch(this.handleError);
  }

  public getFollowById(followId: number): Observable<Follow> {
    return this.http
      .get(API_URL + '/suivi/' + followId)
      .map(response => {
        return new Follow(response.json());
      })
      .catch(this.handleError);
  }

  public updateFollow(follow: Follow): Observable<Follow> {
    return this.http
      .put(API_URL + '/suivi/' + follow.id, follow)
      .map(response => {
        return new Follow(response.json());
      })
      .catch(this.handleError);
  }

  public deleteFollowById(followId: number): Observable<null> {
    return this.http
      .delete(API_URL + '/suivi/' + followId)
      .map(response => null)
      .catch(this.handleError);
  }


  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

}
