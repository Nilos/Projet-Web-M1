import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ProfilComponent } from './profil/profil.component';
import { UserComponent } from './user/user.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { NewCourseComponent } from './new-course/new-course.component';
import { NewSessionComponent } from './new-session/new-session.component';
import { FollowStudentComponent } from './follow-student/follow-student.component';
import { CoursesComponent } from './courses/courses.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { HttpModule } from '@angular/http';
import { UpdateSessionComponent } from './update-session/update-session.component';
import { UpdateCourseComponent } from './update-course/update-course.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CourseComponent } from './course/course.component';
import { NewQuestionComponent } from './new-question/new-question.component';
import { TeacherCourseComponent } from './teacher-course/teacher-course.component';
import { Step2Component } from './step2/step2.component';
import { PreviewComponent } from './preview/preview.component';
import { UpdateFollowComponent } from './update-follow/update-follow.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ProfilComponent,
    UserComponent,
    UpdatePasswordComponent,
    NewCourseComponent,
    NewSessionComponent,
    FollowStudentComponent,
    CoursesComponent,
    CourseComponent,
    UpdateSessionComponent,
    UpdateCourseComponent,
    TeacherCourseComponent,
    Step2Component,
    PreviewComponent,
    NewQuestionComponent,
    UpdateFollowComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
