import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {MeetingDataService} from "../service/meeting-data.service";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-update-session',
  templateUrl: './update-session.component.html',
  styleUrls: ['./update-session.component.css']
})
export class UpdateSessionComponent implements OnInit {

  id;
  session;
  public editorContent: string = '';
  sessionForm: FormGroup;
  titre:string = '';

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private meetingDataService: MeetingDataService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.meetingDataService
        .getMeetingById(this.id)
        .subscribe(
            (session) => {
              this.session = session;
              console.log(this.session);
              this.editorContent = this.session.contenu;
            }
        );

      this.sessionForm = this.formBuilder.group({
          'titre' : [null, Validators.required],
      })
  }

    onFormSubmit(form) {
        this.session.titre=form.titre;
        this.session.contenu = this.editorContent;
        this.session.cours = this.session.cours.id;
        this.meetingDataService.updateMeeting(this.session).subscribe();
        this.router.navigate(['/teacher/course/'+this.session.cours]);

    }

}
