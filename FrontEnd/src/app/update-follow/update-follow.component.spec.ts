import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFollowComponent } from './update-follow.component';

describe('UpdateFollowComponent', () => {
  let component: UpdateFollowComponent;
  let fixture: ComponentFixture<UpdateFollowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFollowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
