import { Component, OnInit } from '@angular/core';
import {CoursesDataService} from "../service/courses-data.service";
import {ActivatedRoute} from "@angular/router";
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { environment } from '../../environments/environment';
import { Courses } from '../model/courses';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Http} from "@angular/http";

@Component({
  selector: 'app-new-course',
  templateUrl: './new-course.component.html',
  styleUrls: ['./new-course.component.css']
})
export class NewCourseComponent implements OnInit {

  public titre_cours: string = '';
  public description: string = '';

  constructor(private http: Http, private route: ActivatedRoute, private coursesDataService: CoursesDataService, private formBuilder: FormBuilder, private router: Router) { }
  courses=new Courses();
  titre:string = '';

  ngOnInit() {
  }

  addToLocal(){
    window.localStorage.setItem("titre_cours", this.titre_cours);
    window.localStorage.setItem("description", this.description);
  }
}
