import { Component, OnInit } from '@angular/core';
import {CoursesDataService} from '../service/courses-data.service';
import {Courses} from "../model/courses";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public editorContent: string = 'My Document\'s Title';
  courses: Courses[] = [];

  constructor( private coursesDataService: CoursesDataService )
  {

  }

  public ngOnInit() {
    this.coursesDataService
        .getAllCourses()
        .subscribe(
            (courses) => {
              this.courses = courses;
              console.log(this.courses);
            }
        );
  }

  onClickMe() {
    alert(this.editorContent);
  }

}
