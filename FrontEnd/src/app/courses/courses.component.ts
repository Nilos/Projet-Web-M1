import { Component, OnInit } from '@angular/core';
import {CoursesDataService} from '../service/courses-data.service';
import { Courses } from '../model/courses';
import { User } from '../model/user';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
  providers: [CoursesDataService]
})
export class CoursesComponent implements OnInit {

  courses: Courses[] = [];

  constructor( private coursesDataService: CoursesDataService )
  {

  }

  public ngOnInit() {
      this.coursesDataService
          .getAllCourses()
          .subscribe(
              (courses) => {
                  this.courses = courses;
                  console.log(this.courses);
              }
          );
  }

  onAddCourses(courses) {
    this.coursesDataService
      .addCourses(courses)
      .subscribe(
      (newCourses) => {
        this.courses = this.courses.concat(newCourses);
      }
      );
  }

  onRemoveCourses(courses) {
    this.coursesDataService
      .deleteCoursesById(courses.id)
      .subscribe(
        (_) => {
          this.courses = this.courses.filter((t) => t.id !== courses.id);
        }
      );
  }

}
