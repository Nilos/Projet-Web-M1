import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {
  private titre_cours: string;
  private titre_seance: string;
  private contenu: string;
  private description: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.titre_cours = window.localStorage.getItem("titre_cours");
    this.description = window.localStorage.getItem("description");
    this.titre_seance = window.localStorage.getItem("titre_seance");
    this.contenu = window.localStorage.getItem("contenu");
  }

}
