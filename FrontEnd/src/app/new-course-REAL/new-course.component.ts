import { Component, OnInit } from '@angular/core';
import {CoursesDataService} from "../service/courses-data.service";
import {ActivatedRoute} from "@angular/router";
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import {Courses} from "../model/courses";

@Component({
  selector: 'app-new-course',
  templateUrl: './new-course.component.html',
  styleUrls: ['./new-course.component.css']
})
export class NewCourseComponentReal implements OnInit {

  constructor(private route: ActivatedRoute, private coursesDataService: CoursesDataService, private formBuilder: FormBuilder, private router: Router) { }
  courses=new Courses();
  courseForm: FormGroup;
  titre:string = '';
  description:string = '';

  ngOnInit() {
    this.courseForm = this.formBuilder.group({
      'titre' : [null, Validators.required],
      'description' : [null, Validators.required]
    })

  }


  onFormSubmit(form) {
    this.courses.titre=form.titre;
    this.courses.description=form.description;
    this.courses.image="chouette.jpg";
    this.courses.actif=true;
    this.courses.user=2;

    this.coursesDataService.addCourses(this.courses).subscribe();
    console.log(this.courses);



  }

}
