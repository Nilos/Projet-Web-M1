import { Component, OnInit } from '@angular/core';
import {CoursesDataService} from "../service/courses-data.service";
import {Router, ActivatedRoute} from "@angular/router";
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Courses} from "../model/courses";

@Component({
  selector: 'app-update-course',
  templateUrl: './update-course.component.html',
  styleUrls: ['./update-course.component.css']
})
export class UpdateCourseComponent implements OnInit {

  constructor(private route: ActivatedRoute, private coursesDataService: CoursesDataService, private formBuilder: FormBuilder, private router: Router) { }
  courses;
  courseForm: FormGroup;
  titre:string = '';
  description:string = '';
  actif:boolean = true;

  ngOnInit() {
    this.getCourse(this.route.snapshot.params['id']);
    this.courseForm = this.formBuilder.group({
      'titre' : [null, Validators.required],
      'description' : [null, Validators.required],
      'actif' : [null, Validators.required]
    })

  }

  getCourse(id){
    this.coursesDataService
      .getCoursesById(id)
      .subscribe(
        (courses) => {
          this.courses = courses;
          console.log(this.courses);
          this.courseForm.setValue({
            titre:courses.titre,
            description:courses.description,
            actif:(courses.actif != false )
          })
        }
      );
  }

  onFormSubmit(form) {
    this.courses.titre=form.titre;
    this.courses.description=form.description;
    this.courses.actif=form.actif;
    if (form.actif==false){
      this.courses.actif=0;
    }
    this.courses.user=this.courses.user.id;
    this.coursesDataService.updateCourses(this.courses).subscribe();
    this.router.navigate(['/teacher/course/'+this.courses.id]);
    console.log(form);

}
}
