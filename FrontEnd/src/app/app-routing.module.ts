import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from "./home/home.component";
import { RegisterComponent } from "./register/register.component";
import {ProfilComponent} from "./profil/profil.component";
import {UpdatePasswordComponent} from "./update-password/update-password.component";
import {NewCourseComponent} from "./new-course/new-course.component";
import {NewSessionComponent} from "./new-session/new-session.component";
import {FollowStudentComponent} from "./follow-student/follow-student.component";
import {CoursesComponent} from "./courses/courses.component";
import {CourseComponent} from "./course/course.component";
import {UpdateSessionComponent} from "./update-session/update-session.component";
import {UpdateCourseComponent} from "./update-course/update-course.component";
import {TeacherCourseComponent} from "./teacher-course/teacher-course.component";
import {Step2Component} from "./step2/step2.component";
import {PreviewComponent} from "./preview/preview.component";
import {NewQuestionComponent} from "./new-question/new-question.component";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'profil',
        component: ProfilComponent,
    },
    {
        path: 'courses',
        component: CoursesComponent,
    },
    {
        path: 'course/add',
        component: NewCourseComponent,
    },{
        path: 'course/update/:id',
        component: UpdateCourseComponent,
    },
    {
        path: 'course/:id',
        component: CourseComponent,
    },
    {
        path: 'teacher/course/:id',
        component: TeacherCourseComponent,
    },
    {
        path: 'course/:id/session/add',
        component: NewSessionComponent,
    },{
        path: 'session/:id/question/add',
        component: NewQuestionComponent,
    },
    {
        path: 'session/update/:id',
        component: UpdateSessionComponent,
    },
    {
        path: 'follow_student',
        component: FollowStudentComponent,
    },
    {
        path: 'reset_password',
        component: UpdatePasswordComponent,
    },
    {
        path: 'step2',
        component: Step2Component,
    },
    {
        path: 'preview',
        component: PreviewComponent,
    },
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: '**', redirectTo: '' },
];

@NgModule({
    imports: [
    CommonModule,
    RouterModule.forRoot(routes)
    ],
    exports: [RouterModule],
    declarations: []
})

export class AppRoutingModule { }
