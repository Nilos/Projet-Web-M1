import { Component, OnInit } from '@angular/core';
import {User} from "../model/user";
import {Courses} from "../model/courses";

@Component({
  selector: 'app-follow-student',
  templateUrl: './follow-student.component.html',
  styleUrls: ['./follow-student.component.css']
})
export class FollowStudentComponent implements OnInit {

  public selectedValue;

  courses : Array<Courses>;

  constructor() { }

  ngOnInit() {
  }

  users : Array<User>;

  onChange() {

  }

}
