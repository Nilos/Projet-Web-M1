import { User } from './user';
import { Meeting } from './meeting';

export class Follow {
  id: number;
  note: number;
  commentaire: string;
  createAt: string;
  user: User;
  seance: Meeting;
    
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}