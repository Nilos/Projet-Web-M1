import { User } from './user';
import {Meeting} from "./meeting";

export class Courses {
  id: number;
  titre: string;
  description: string;
  actif: boolean;
  image: string;
  user: number;
  seances: Meeting[];

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
