export class User {
  id: number;
  email: string;
  nom: string;
  prenom: boolean;
  tel: string;
  password: string;
  profil: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}