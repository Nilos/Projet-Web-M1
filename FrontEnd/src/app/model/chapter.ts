export class Chapter {
  id: number;
  email: string;
  nom: string;
  prenom: boolean;
  tel: string;
  password: string;
  
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}