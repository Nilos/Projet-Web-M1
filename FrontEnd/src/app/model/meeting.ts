import { Courses } from './courses';

export class Meeting {
  id: number;
  titre: string;
  contenu: string;
  cours: number;
  
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}