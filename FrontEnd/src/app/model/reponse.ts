import { Question } from './question';

export class Reponse {
  id: number;
  libelle: string;
  correcte: boolean;
  question: Question;
    
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}