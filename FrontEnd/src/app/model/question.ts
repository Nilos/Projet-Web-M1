import { Meeting } from './meeting';

export class Question {
  id: number;
  libelle: string;
  seance: Meeting;
    
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}