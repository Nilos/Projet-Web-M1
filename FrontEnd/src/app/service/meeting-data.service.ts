import { Injectable } from '@angular/core';
import { Meeting } from '../model/meeting';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class MeetingDataService {

  constructor(
    private api: ApiService
  ) {
  }

  // Simulate POST /courses
  addMeeting(meeting: Meeting): Observable<Meeting> {
    return this.api.createMeeting(meeting);
  }

  // Simulate DELETE /courses/:id
  deleteMeetingById(meetingId: number): Observable<Meeting> {
    return this.api.deleteMeetingById(meetingId);
  }

  // Simulate PUT /courses/:id
  updateMeeting(meeting: Meeting): Observable<Meeting> {
    return this.api.updateMeeting(meeting);
  }

  // Simulate GET /courses
  getAllMeetings(): Observable<Meeting[]> {
    return this.api.getAllMeetings();
  }

  // Simulate GET /courses/:id
  getMeetingById(meetingId: number): Observable<Meeting> {
    return this.api.getMeetingById(meetingId);
  }
}
