import { Injectable } from '@angular/core';
import { Follow } from '../model/follow';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class FollowDataService {

  constructor(
    private api: ApiService
  ) {
  }

  // Simulate POST /courses
  addFollow(follow: Follow): Observable<Follow> {
    return this.api.createFollow(follow);
  }

  // Simulate DELETE /courses/:id
  deleteFollowById(followId: number): Observable<Follow> {
    return this.api.deleteFollowById(followId);
  }

  // Simulate PUT /courses/:id
  updateFollow(follow: Follow): Observable<Follow> {
    return this.api.updateFollow(follow);
  }

  // Simulate GET /courses
  getAllFollows(): Observable<Follow[]> {
    return this.api.getAllFollows();
  }

  // Simulate GET /courses/:id
  getFollowById(followId: number): Observable<Follow> {
    return this.api.getFollowById(followId);
  }
}
