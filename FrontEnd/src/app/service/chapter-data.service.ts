import { Injectable } from '@angular/core';
import { Chapter } from '../model/chapter';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class ChapterDataService {

  constructor(
    private api: ApiService
  ) {
  }

  // Simulate POST /courses
  addChapter(chapter: Chapter): Observable<Chapter> {
    return this.api.createChapter(chapter);
  }

  // Simulate DELETE /courses/:id
  deleteChapterById(chapterId: number): Observable<Chapter> {
    return this.api.deleteChapterById(chapterId);
  }

  // Simulate PUT /courses/:id
  updateChapter(chapter: Chapter): Observable<Chapter> {
    return this.api.updateChapter(chapter);
  }

  // Simulate GET /courses
  getAllChapters(): Observable<Chapter[]> {
    return this.api.getAllChapters();
  }

  // Simulate GET /courses/:id
  getChapterById(chapterId: number): Observable<Chapter> {
    return this.api.getChapterById(chapterId);
  }
}
