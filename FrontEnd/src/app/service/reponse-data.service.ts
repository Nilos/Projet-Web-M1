import { Injectable } from '@angular/core';
import { Reponse } from '../model/reponse';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class ReponseDataService {

  constructor(
    private api: ApiService
  ) {
  }

  // Simulate POST /courses
  addReponse(reponse: Reponse): Observable<Reponse> {
    return this.api.createReponse(reponse);
  }

  // Simulate DELETE /courses/:id
  deleteReponseById(reponseId: number): Observable<Reponse> {
    return this.api.deleteReponseById(reponseId);
  }

  // Simulate PUT /courses/:id
  updateReponse(reponse: Reponse): Observable<Reponse> {
    return this.api.updateReponse(reponse);
  }

  // Simulate GET /courses
  getAllReponses(): Observable<Reponse[]> {
    return this.api.getAllReponses();
  }

  // Simulate GET /courses/:id
  getReponseById(reponseId: number): Observable<Reponse> {
    return this.api.getReponseById(reponseId);
  }
}
