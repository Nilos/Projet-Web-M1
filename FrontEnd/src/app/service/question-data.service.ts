import { Injectable } from '@angular/core';
import { Question } from '../model/question';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class QuestionDataService {

  constructor(
    private api: ApiService
  ) {
  }

  // Simulate POST /courses
  addQuestion(question: Question): Observable<Question> {
    return this.api.createQuestion(question);
  }

  // Simulate DELETE /courses/:id
  deleteQuestionById(questionId: number): Observable<Question> {
    return this.api.deleteQuestionById(questionId);
  }

  // Simulate PUT /courses/:id
  updateQuestion(question: Question): Observable<Question> {
    return this.api.updateQuestion(question);
  }

  // Simulate GET /courses
  getAllQuestions(): Observable<Question[]> {
    return this.api.getAllQuestions();
  }

  // Simulate GET /courses/:id
  getQuestionById(questionId: number): Observable<Question> {
    return this.api.getQuestionById(questionId);
  }
}
