import { TestBed, inject } from '@angular/core/testing';

import { ReponseDataService } from './reponse-data.service';

describe('ReponseDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReponseDataService]
    });
  });

  it('should be created', inject([ReponseDataService], (service: ReponseDataService) => {
    expect(service).toBeTruthy();
  }));
});
