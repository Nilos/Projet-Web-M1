import { Injectable } from '@angular/core';
import { Courses } from '../model/courses';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class CoursesDataService {

  constructor(
    private api: ApiService
  ) {
  }

  // Simulate POST /courses
  addCourses(courses: Courses): Observable<Courses> {
    return this.api.createCourses(courses);
  }

  // Simulate DELETE /courses/:id
  deleteCoursesById(coursesId: number): Observable<Courses> {
    return this.api.deleteCoursesById(coursesId);
  }

  // Simulate PUT /courses/:id
  updateCourses(courses: Courses): Observable<Courses> {
    return this.api.updateCourses(courses);
  }

  // Simulate GET /courses
  getAllCourses(): Observable<Courses[]> {
    return this.api.getAllCourses();
  }

  // Simulate GET /courses/:id
  getCoursesById(coursesId: number): Observable<Courses> {
    return this.api.getCoursesById(coursesId);
  }
}
