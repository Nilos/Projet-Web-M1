import { TestBed, inject } from '@angular/core/testing';

import { FollowDataService } from './follow-data.service';

describe('FollowDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FollowDataService]
    });
  });

  it('should be created', inject([FollowDataService], (service: FollowDataService) => {
    expect(service).toBeTruthy();
  }));
});
