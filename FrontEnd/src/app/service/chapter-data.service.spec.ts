import { TestBed, inject } from '@angular/core/testing';

import { ChapterDataService } from './chapter-data.service';

describe('ChapterDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChapterDataService]
    });
  });

  it('should be created', inject([ChapterDataService], (service: ChapterDataService) => {
    expect(service).toBeTruthy();
  }));
});
