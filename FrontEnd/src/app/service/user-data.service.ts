import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';


@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(
    private api: ApiService
  ) {
  }

  // Simulate POST /courses
  addUser(user: User): Observable<User> {
    return this.api.createUser(user);
  }

  // Simulate DELETE /courses/:id
  deleteUserById(userId: number): Observable<User> {
    return this.api.deleteUserById(userId);
  }

  // Simulate PUT /courses/:id
  updateUser(user: User): Observable<User> {
    return this.api.updateUser(user);
  }

  // Simulate GET /courses
  getAllUsers(): Observable<User[]> {
    return this.api.getAllUsers();
  }

  // Simulate GET /courses/:id
  getUserById(userId: number): Observable<User> {
    return this.api.getUserById(userId);
  }
}
