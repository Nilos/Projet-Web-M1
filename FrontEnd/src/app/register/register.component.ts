import { Component, OnInit } from '@angular/core';
import {User} from "../model/user";
import {FormBuilder, Validators, FormGroup} from "@angular/forms";
import {UserDataService} from "../service/user-data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = new User();
  private subscribeForm: FormGroup;

  constructor(private formBuilder: FormBuilder,private userDataService: UserDataService,private router:Router) { }

  ngOnInit() {
    this.subscribeForm = this.formBuilder.group({
      'email' : [null, Validators.required],
      'nom' : [null, Validators.required],
      'prenom' : [null, Validators.required],
      'actif' : [null, Validators.required],
      'tel' : [null, Validators.required],
      'password' : [null, Validators.required],
      'passwordConfirm' : [null, Validators.required],
    })
  }

  onFormSubmit(form) {
    if(form.password != form.passwordConfirm){
      alert("les mots de passe ne correspondent pas !");
    }
    else{
      this.user.email=form.email;
      this.user.nom=form.nom;
      this.user.prenom=form.prenom;
      if (form.actif==false || form.actif === null){
        this.user.profil= "etudiant";
      }
      else{
        this.user.profil="professeur";
      }
      this.user.tel=form.tel;
      this.user.password = form.password;
    }
    this.userDataService.addUser(this.user).subscribe();
    this.router.navigate(['/courses']);
    console.log(form);
    console.log(this.user);

  }

}
