import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {CoursesDataService} from "../service/courses-data.service";

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  id: number;
  acourse;
  montest = false;

  constructor( private route: ActivatedRoute,
               private router: Router,
               private coursesDataService: CoursesDataService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.coursesDataService
        .getCoursesById(this.id)
        .subscribe(
            (courses) => {
              this.acourse = courses;
              console.log(this.acourse);
            }
        );
  }

}
