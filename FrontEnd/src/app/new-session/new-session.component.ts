import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import {MeetingDataService} from "../service/meeting-data.service";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {Meeting} from "../model/meeting";

@Component({
  selector: 'app-new-session',
  templateUrl: './new-session.component.html',
  styleUrls: ['./new-session.component.css']
})
export class NewSessionComponent implements OnInit {

  id_course;
  sessionForm: FormGroup;
  titre:string = '';
  public editorContent: string = 'Rédigez le contenu de votre cours ici.';
  session = new Meeting();

  constructor(private formBuilder: FormBuilder,
              private meetingDataService: MeetingDataService,
              private route: ActivatedRoute,
              private router: Router,) { }

  ngOnInit() {
    this.id_course = this.route.snapshot.params['id'];
    this.sessionForm = this.formBuilder.group({
      'titre' : [null, Validators.required],
    })
  }

  onFormSubmit(form) {
    this.session.titre=form.titre;
    this.session.contenu = this.editorContent;
    this.session.cours = this.id_course;
    this.meetingDataService.addMeeting(this.session).subscribe();
    this.router.navigate(['/teacher/course/'+this.session.cours]);
    console.log(this.session);

  }

}
