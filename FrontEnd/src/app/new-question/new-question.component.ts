import { Component, OnInit } from '@angular/core';
import {CoursesDataService} from "../service/courses-data.service";
import {ActivatedRoute} from "@angular/router";
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Courses} from "../model/courses";

@Component({
  selector: 'app-new-question',
  templateUrl: './new-question.component.html',
  styleUrls: ['./new-question.component.css']
})
export class NewQuestionComponent implements OnInit {

  constructor(private route: ActivatedRoute, private coursesDataService: CoursesDataService, private formBuilder: FormBuilder) { }
  courses=new Courses();
  courseForm: FormGroup;
  titre:string = '';
  description:string = '';

  ngOnInit() {
    this.courseForm = this.formBuilder.group({
      'titre' : [null, Validators.required],
      'description' : [null, Validators.required]
    })

  }


  onFormSubmit(form) {
    this.courses.titre=form.titre;
    this.courses.description=form.description;
    this.coursesDataService.addCourses(this.courses).subscribe();
    console.log(this.courses);

  }

}
