import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CoursesDataService} from "../service/courses-data.service";
import {MeetingDataService} from "../service/meeting-data.service";

@Component({
  selector: 'app-teacher-course',
  templateUrl: './teacher-course.component.html',
  styleUrls: ['./teacher-course.component.css']
})
export class TeacherCourseComponent implements OnInit {

  id: number;
  acourse;
  montest = false;

  constructor( private route: ActivatedRoute,
               private router: Router,
               private coursesDataService: CoursesDataService,
               private meetingDataService: MeetingDataService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.coursesDataService
        .getCoursesById(this.id)
        .subscribe(
            (courses) => {
              this.acourse = courses;
              console.log(this.acourse);
            }
        );
  }

  delete_session(seance){
      this.meetingDataService
          .deleteMeetingById(seance.id)
          .subscribe(
              (_) => {
                  this.acourse.seances = this.acourse.seances.filter((t) => t.id !== this.acourse.id);
              }
          );
  }

}
