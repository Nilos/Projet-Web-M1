import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Courses} from "../model/courses";
import {Meeting} from "../model/meeting";
import {MeetingDataService} from "../service/meeting-data.service";
import {CoursesDataService} from "../service/courses-data.service";
import {FormGroup, Validators, FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.css']
})
export class Step2Component implements OnInit {

  id;
  titre_cours: string = '';
  description: string = '';
  titre_seance: string= '';
  public editorContent: string = 'Votre contenu';
  cours = new Courses();
  seance = new Meeting();
  coursForm: FormGroup;

  constructor(private formBuilder: FormBuilder,private route: ActivatedRoute, private meetingDataService: MeetingDataService, private coursesDataService: CoursesDataService, private router: Router) { }

  ngOnInit() {
    this.coursForm = this.formBuilder.group({
      'titre' : [null, Validators.required],
    });
    this.titre_cours = window.localStorage.getItem("titre_cours");
    this.description = window.localStorage.getItem("description");
  }

  onFormSubmit(form) {
    this.cours.titre= this.titre_cours;
    this.cours.description = this.description;
    this.cours.actif = true;
    this.cours.image = "chouette.jpg";
    this.cours.user = 12;

    this.seance.titre = form.titre;
    console.log(form.titre);
    this.seance.contenu = this.editorContent;
    this.coursesDataService.addCourses(this.cours).subscribe(
        (courses) => {
          this.seance.cours = courses.id;
          console.log(this.seance.cours);
          this.meetingDataService.addMeeting(this.seance).subscribe();
          this.router.navigate(['/courses']);

        }
    );

  }

  preview(){
    window.localStorage.setItem("titre_seance", this.titre_seance);
    window.localStorage.setItem("contenu", this.editorContent);
  }

}
